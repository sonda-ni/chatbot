FROM python:3

WORKDIR /usr/src/app

RUN pip3 install flask tensorflow keras nltk matplotlib pyspellchecker 

CMD [ "python", "./app.py" ]