# ChatBot Platform

This project is a Messenger Platform using machine learning.
by Renan Soares Fernandes and SONDA.

Contact: GitHub: https://github.com/o0renan0o, 
         E-mail: renan-soares@live.com.
         
**The API has 2 routes:**

**/generate_model:**

Passing all data base to train model again as following
Return a picture with the results of training data.

Node:
```javascript
let train_machine_learning = async () => {
    let data = JSON.stringify({"intents": await knowledge_database.get_dataBase()});

    let config = {
        method: 'post',
        url: 'http://127.0.0.1:5500/generate_model',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    return await axios(config)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error
        });

}
```

**/predict:**

Predict message from the trained model
Return a message of the output knowledge database.


Node:
```javascript
let predict_machine_learning = async (message) => {
    let config = {
        method: 'post',
        url: 'http://127.0.0.1:5500/predict',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            "message": message
        }
    };

    return await axios(config)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error
        });
}
```