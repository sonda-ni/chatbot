from flask import Flask, jsonify, request
import nltk

from tensor_teach.dnn import generate_model
from tensor_predict.dnn import predict
import json
import os

nltk.download('punkt')

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
app = Flask(__name__)


@app.route('/generate_model', methods=['POST'])
def model():
    data = json.loads(request.data)
    try:
        doc = str(generate_model(data))
        return jsonify({
            "keras_status": "Learned",
            "graph": doc
        })
    except:
        return 'Error learning model.'


@app.route('/predict', methods=['POST'])
def predict_text():
    try:
        data = predict(request.json['message'])
        return jsonify({
            "keras_predict_status": data['status'],
            "message": data['output'],
            "bot_function": data['function'],
            "_id": data['id_best_match'],
            "tag": data['tag'],
            "precision": data['MESSENGER_PRECISION']
        })
    except ValueError:
        print(ValueError)
        return jsonify({'keras_predict_status': 'Error.'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5500)
