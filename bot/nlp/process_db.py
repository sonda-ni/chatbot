import nltk
from nltk.stem.lancaster import LancasterStemmer

stemmer = LancasterStemmer()

import numpy
import pickle


# Process with NLTK, working with tokenizer and stemmer.
# It generates a pickle data and return (words, subject, x_data, y_data, all_knowledge).
def nlp_generate_pickle(knowledge_data):
    # Used to classify CNNs and retrieve data from output.
    subject = []
    # Get a bag of tokens
    bag_of_tokens = []
    # Step to generate a bag.
    array_x_init = []
    array_y_init = []
    # Get all knowledge for further usage in prediction too.
    all_knowledge = knowledge_data["intents"]

    for base in all_knowledge:
        for pattern in base["inputs"]:
            tokens = nltk.word_tokenize(pattern)
            bag_of_tokens.extend(tokens)
            bag_of_tokens.extend([base["key"]])
            array_x_init.append(tokens)
            array_y_init.append(base["subject"])

        if base["subject"] not in subject:
            subject.append(base["subject"])

    bag_of_tokens = [stemmer.stem(w.lower()) for w in bag_of_tokens if w != "?"]
    bag_of_tokens = sorted(list(set(bag_of_tokens)))

    subject = sorted(subject)

    # Process to generate a BAG_OF_WORDS to feed a neural network.
    y_list_zeros = [0 for _ in range(len(subject))]
    x_data = []
    y_data = []
    for index, knowledge_elements in enumerate(array_x_init):
        nlp_bag_of_words = []

        output_list = y_list_zeros[:]
        output_list[subject.index(array_y_init[index])] = 1

        tokens = [stemmer.stem(words.lower()) for words in knowledge_elements]
        for inner_word in bag_of_tokens:
            if inner_word in tokens:
                nlp_bag_of_words.append(1)
            else:
                nlp_bag_of_words.append(0)

        x_data.append(nlp_bag_of_words)
        y_data.append(output_list)

    # Data to neural network, X and Y.
    x_data = numpy.array(x_data)
    y_data = numpy.array(y_data)

    # Pickle for quick tensor_predict in nlp/dnn
    with open("knowledge.pickle", "wb") as f:
        pickle.dump((bag_of_tokens, subject, x_data, y_data, all_knowledge), f)

    return bag_of_tokens, subject, x_data, y_data, all_knowledge
