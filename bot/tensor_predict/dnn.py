import nltk
from nltk.stem.lancaster import LancasterStemmer

stemmer = LancasterStemmer()

import numpy
import os
import pickle
import random
from tensorflow.keras.models import load_model
from spellchecker import SpellChecker

spell = SpellChecker(language='pt')


# Generate a bag of words from texts coming in.
def bag_of_words(s, words):
    bags = [0 for _ in range(len(words))]

    s_words = nltk.word_tokenize(s)
    misspelled = spell.unknown(s_words)
    for word in misspelled:
        if word in s_words:
            s_words.remove(word)
            s_words.append(spell.correction(word))
    s_words = [stemmer.stem(word.lower()) for word in s_words]

    for se in s_words:
        for i, w in enumerate(words):
            if w == se:
                bags[i] = 1

    return numpy.array([bags])


# predict using model from keras.
def predict(text):
    try:
        model = load_model(os.path.abspath("model.h5"))
        print("Model loaded.")
    except:
        return "Model load error."

    try:
        with open("knowledge.pickle", "rb") as f:
            inner_words, subject, x_data, y_data, all_knowledge = pickle.load(f)
    except:
        return "Pickle load error."

    # Spell Correction
    s_words = nltk.word_tokenize(text)
    misspelled = spell.unknown(s_words)
    baag = []
    correct = []
    for word in misspelled:
        # Get the one `most likely` answer
        correct.append(spell.correction(word))
        baag.append(spell.candidates(word))
    print("")

    # Predict
    results = model.predict(bag_of_words(text, inner_words))
    results_index = numpy.argmax(results)
    index_value = numpy.max(results)
    tag = subject[results_index]

    # If there is no enough ratings, reply no_reply
    if index_value <= 0.15:
        return low_rating_no_reply(all_knowledge)

    # Return proper action
    for knowledge_base in all_knowledge:
        if knowledge_base['subject'] == tag:
            # Start Function
            if knowledge_base["bot_function"] is not "":
                return {
                    "status": 200,
                    "output": "Start Function",
                    "function": knowledge_base["bot_function"],
                    "service": 'From Keras',
                    "id_best_match": str(knowledge_base['_id']),
                    "tag": tag,
                    "MESSENGER_PRECISION": str((index_value * 100)) + "%"
                }
            # Send Message
            else:
                return {
                    "status": "200",
                    "output": random.choice(knowledge_base['output']),
                    "function": "No Functions",
                    "service": 'From Keras',
                    "id_best_match": str(knowledge_base['_id']),
                    "tag": tag,
                    "MESSENGER_PRECISION": str((index_value * 100)) + "%"
                }


def low_rating_no_reply(all_knowledge):
    for knowledge_base in all_knowledge:
        if knowledge_base['subject'] == "no_reply":
            return {
                "tag": "no_reply",
                "output": random.choice(knowledge_base['output'])
            }
