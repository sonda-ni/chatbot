import pickle
import base64
from io import BytesIO
import nlp.process_db as nlp
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import Callback
from keras.callbacks import EarlyStopping
from matplotlib import pyplot as plot


# A class to limit a learning process, avoiding learning overfitting.
class MyCallback(Callback):
    def on_epoch_end(self, epoch, logs={}):
        if logs.get('loss') < 0.002:
            print("\nWe have reached %2.2f%% of loss, so we will stopping training." % (logs.get('loss') * 100))
            self.model.stop_training = True


# function to teach a new model from keras using pickle to quick retrieve data.
def generate_model(node_data, img=None):
    callbacks = MyCallback()
    early_stop = EarlyStopping(monitor='loss', patience=5)

    try:
        inner_words, subject, x_data, y_data, all_knowledge = nlp.nlp_generate_pickle(node_data)
    except:
        return "Error generating pickle."

    # Recurrent Neural Network
    model = keras.Sequential()
    model.add(layers.Dense(30, input_shape=(len(x_data[0]),)))
    model.add(layers.Dense(30, ))
    model.add(layers.Dense(30, ))
    model.add(layers.Dense(len(y_data[0])))
    model.add(layers.Activation('softmax'))
    opt = keras.optimizers.SGD(learning_rate=node_data["learning_rate"])
    model.compile(optimizer=opt, loss='mean_squared_error', metrics=['accuracy'])
    model.summary()
    history = model.fit(x_data, y_data, epochs=node_data["epochs"], batch_size=5, verbose=2, callbacks=[callbacks, early_stop])

    # Plot Data
    history_dict = history.history
    loss_values = history_dict['loss']
    accuracy_values = history_dict['accuracy']
    plot.plot(loss_values, 'bo', label='Loss')
    plot.plot(accuracy_values, 'r', label='Accuracy')
    plot.ylabel('Percentage')
    plot.xlabel('Epochs')
    plot.legend(framealpha=1, frameon=True)

    # Get base64 to send.
    buf = BytesIO()
    plot.savefig(buf, format='png')
    buf.seek(0)
    pic_hash = base64.b64encode(buf.getvalue()).decode('utf-8')
    buf.close()
    plot.close()

    # Save and send results.
    try:
        model.save("model.h5")
        with open("knowledge.pickle", "wb") as f:
            pickle.dump((inner_words, subject, x_data, y_data, all_knowledge), f)
        return pic_hash
    except:
        return "Error saving model."
