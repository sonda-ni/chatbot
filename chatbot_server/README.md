# ChatBot Platform Server -- Node.js

This project is a Messenger Platform built in Node.js 
by Renan Soares Fernandes and SONDA.


This server uses NLP, Machine Learning and Dice's Coefficient.

Libraries:
* NLP -> NLTK -> https://www.nltk.org/
* Keras -> https://keras.io/
* Dice's Coefficient -> https://www.npmjs.com/package/string-similarity

Both of them are free to use.

Contact: GitHub: https://github.com/o0renan0o, 
         E-mail: renan-soares@live.com.