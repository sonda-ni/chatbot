# TRE BD Environment

## Create

Just open the jar file.

## ENDPOINT

http://localhost:8080/eleitor?nome=JOSE%20LOPES%20DE%20SOUZA&nomeMae=MARIA%20LOPES%20DE%20SOUZA&dtNascimento=1955-06-10

## ENDPOINT REQUEST

"nome"
"nomeMae"
"dtNascimento"

## Json Model

{
    "id": "003262491392",
    "nome": "JOSE LOPES DE SOUZA",
    "nomeMae": "MARIA LOPES DE SOUZA",
    "dtNascimento": "1955-06-10",
    "situacao": "REGULAR",
    "numZona": 68,
    "municipio": "RURÓPOLIS",
    "bairro": "ZONA RURAL",
    "enderecoZona": "AV JUSCELINO KUBITSCHECK, 325",
    "localVotacao": "ESCOLA MONTE CASTELO",
    "enderecoLocal": "RODOVIA TRANSAMAZONICA, KM 112 , ITAITUBA/RUROPLOIS, S/N",
    "numSecao": 43
}
