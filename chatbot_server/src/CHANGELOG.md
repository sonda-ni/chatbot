# Change Log

## December 10, 2019

* Implemented v1.0 features including replies and new structure.

## December 21, 2019

* Mongodb json schema to attend necessary features and new functions.

## December 30, 2019

* Changing the logic of the robot, making the turing module leads the 
user message through the possibilities tree.

## January 06, 2020

* Ending the robot, and implementing the last features like clerk modules and
separating the syntaxanalizer from all other modules. Polishing up the whole code
making it better for all purposes.
* FaceBook testing. Webhook OK.

## January 10, 2020

* Testing and polishing up te code. Fixing bugs and changing some
json structures for a better code. Teaching new message entries for 
the robot. 
* Looking for the new webhooks like, Whatsapp, instagran and AJAX for website.

## January 15, 2020

Large changes were done. 
* Variables names from the robot root and whole tree of possibilities that the variable ands up.
* The code was simplified, taking the hard code out.
* TESTING THE SOFTWARE IN ANY POSSIBLE WAY AGAINST BUGS.

## January 24, 2020

* Done
* Frontend and TRE DB is finished.
* Lots of modifications through the code and adapt it with to avoid bugs using promises.
* PM2 installed to run in daemon mode. 

## January 28, 2020

* Building frontend.
* Restructuring the code.

## February 04, 2020

* Mongoose implementation success.
* Migrating to microservice architecture.

## February 05, 2020

* Robot is now a service.
* The whole application ends up and starts with gateway.
* Code is ready with new structure.

## February 17, 2020

* .env is now where to store configuration.
* Frontend is almost done.
* Strong security with JWT, CORS and authentication.
* Building the teaching part.

## February 21, 2020

* Chat Robot V2 is done.
* Jest Tests where implemented.
* New Frontend with Material Bootstrap.
* New Dashboard and find for users where implemented as well.
* Backend modifications end better codding.
* New data mongoose module (dashboard)