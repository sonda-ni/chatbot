# ChatBot Platform -- node.js

This project is a Messenger Platform built in Node.js 
by Renan Soares Fernandes and SONDA.

Contact: GitHub: https://github.com/o0renan0o, 
         E-mail: renan-soares@live.com.

With this app, you can send it messages and it will reply like a person.
The graph v2.6 is used, https://graph.facebook.com/v2.6/me/messages'.
It contains the following functionality:

* Webhook (specifically for Messenger Platform events)
* Send API 
* Web Plugins
* Messenger Platform v1.3 features
* MongoDb
* Express

## Setup

Set the values in `config/default.json` before running the sample. Descriptions of each parameter can be found in `knowledge_filter.js`. Alternatively, you can set the corresponding environment variables as defined in `knowledge_filter.js`.

Replace values for `APP_ID` and `PAGE_ID` in `public/index.html`.

## Run

You can start the server by running `npm start`. However, the webhook must be at a public URL that the Facebook servers can reach. Therefore, running the server locally on your machine will not work.

You can run this example on a cloud service provider like Heroku, Google Cloud Platform or AWS. Note that webhooks must have a valid SSL certificate, signed by a certificate authority.

## Webhook

All webhook code is in `knowledge_filter.js`. It is routed to `/facebook`. This project handles callbacks for authentication, messages, delivery confirmation and postbacks.

## License

GNU Affero General Public License v3.0

## ENV to DOCKER

MONGODB = mongodb://mongo:27017/
