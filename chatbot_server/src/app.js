/*
 * Atenção
 * Certifique-se de configurar o seu CONFIG VALUES antes de iniciar.
 * Modifique usando o config file em /config.
 *
 * Lembre-se de fazer os imports necessários para o node_modules.
 *
 * O validationToken em .env deve ser colocado junto ao link https
 * do servidor criado. As configurações devem ser feitas no webhook no facebook
 * https://developers.facebook.com/apps, criando um APP para atender à conexão.
 *
 * Grande parte do código foi escrito em inglês para padronizacão internacional.
 *
 * A aplicação recebe a requisição e envia para o dice_coeficiente facebook.js, onde começa o tratamento da menssagem enviada.
 *
 * (English)
 * The Robot takes the message at line 239...
 */

const
    express = require('express'),
    bodyParser = require('body-parser'),
    routes = require('./routes/routes');
//ENV
require('dotenv').config();

let app = express();
app.set('port', process.env.PORT || 5000);
app.set('view engine', 'ejs');
app.set('views', 'public/view');
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/', routes);

module.exports = app;