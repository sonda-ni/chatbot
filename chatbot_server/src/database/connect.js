const
    mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    dashboardData = require('../database/models/dashboard'),
    knowledge = require('../database/models/knowledge'),
    fs = require('fs');
admins = require('./models/administradores');

let options = {
    dbName: process.env.DBNAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    //user: process.env.USER,
    //pass: process.env.PASS
};

let uri = process.env.MONGODB;


//Mongoose
mongoose.set('useFindAndModify', false);
mongoose.connect(uri, options)
    .then(() => {
        console.log(`MongoDB Connected`)
    })
    .catch(error => console.log(`MongoDB Connection Error  -> Verify connect.js, DB or authentication
    LOG: ${error}`));


try {
    // Verify if is created or create
    dashboardData.findById(1).then(async document => {
        if (document) {
            console.log('Dashboard Data already created...')
        } else {
            let dashboard = new dashboardData();
            dashboard.save().then(() => {
                console.log('Dashboard data created !')
            })
                .catch(err => console.log(err))
        }
    });

    // Verify if is created or create
    admins.findById(process.env.USER).then(async (document) => {
        if (document) {
            console.log('Admin MASTER already created...')
        } else {
            let salt = await bcrypt.genSalt(8);
            let newUser = new admins({
                _id: process.env.USER,
                firstName: process.env.USER_NAME,
                business: 'master',
                email: process.env.USER,
                //'admin' or 'user'
                access: 'admin',
                password: await bcrypt.hash(process.env.PASS, salt),
            });
            newUser.save()
                .then(() => {
                    console.log("Admin MASTER created...");
                })
                .catch((err) => {
                    console.log(err)
                })

        }
    }).catch(async (err) => {
        console.log(`Error to create or verify ADMIN MASTER
    LOG: ${err}`);
    });
} catch (e) {
    console.log(`Mongoose Error: ${e}`)
}

let json_data = fs.readFileSync('database/initial_knowledge_base.json');
let knowledge_base = JSON.parse(json_data);

let for_all = []
knowledge_base.forEach(data => {
    knowledge.findOne({subject: data['subject']})
        .then(doc => {
            if (!doc) {
                let newKnowledge = {
                    subject: data['subject'],
                    inputs: data['inputs'],
                    key: data['key'],
                    output: data['output'],
                    insertedby: data['insertedby'],
                    summary: data['summary'],
                    bot_function: data['bot_function']
                };
                knowledge.findOneAndUpdate({subject: data['subject']}, newKnowledge).then(async (document) => {
                    if (document) {
                    } else {
                        let newKnowledge = knowledge({
                            subject: data['subject'],
                            inputs: data['inputs'],
                            key: data['key'],
                            output: data['output'],
                            insertedby: data['insertedby'],
                            summary: data['summary'],
                            bot_function: data['bot_function']
                        });
                        newKnowledge.save()
                    }
                }).catch(async (err) => {
                    console.log(`Error to create or verify ADMIN MASTER
    LOG: ${err}`);
                });
            }

        })
})

knowledge.insertMany(for_all)
    .then(function () {
        console.log("Data inserted")  // Success
    }).catch(function (error) {
    console.log(error)      // Failure
});

module.exports = mongoose;