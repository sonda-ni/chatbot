const mongoose = require("mongoose"),

    user = new mongoose.Schema({
        _id: {type: String, trim: true, default:''},
        access_token: {type: String, trim: true, default:''},

        firstName: {type: String, trim: true, default:''},
        lastName: {type: String, trim: true, default:''},
        email: {type: String, required: true, trim: true, default:''},
        password: {type: String, required: true, trim: true, default:''},
        business: {type: String, trim: true, default:''},
        access: {type: String, required: true, trim: true, default:''},
        created: {type: Date, trim: true, default: Date.now()}
    }, {collection: 'admins'});

module.exports = mongoose.model('admins', user);