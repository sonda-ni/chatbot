const mongoose = require("mongoose"),

    user = new mongoose.Schema({
        _id: {type: String, trim: true, default:'master'},
        access_token: {type: String, trim: true, default:''},
        roles: [{role: 'readWrite', db: 'robot'}],

        firstName: {type: String, trim: true, default:'Renan'},
        lastName: {type: String, trim: true, default:'Fernandes'},
        email: {type: String, trim: true, default:'renan-soares@live.com'},
        password: {type: String, trim: true, default:'sonda-bcbk-ejh5-x12d'},
        created: {type: Date, trim: true, default: Date.now()}
    }, {collection: 'master'});

module.exports = mongoose.model('master', user);