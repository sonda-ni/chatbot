const mongoose = require("mongoose"),

    user = new mongoose.Schema({
            _id: {type: Number, default: 1},
            messages: {type: Number, default: 0},
            facebook: {type: Number, default: 0},
            sobretre: {type: Number, default: 0},
            webpage: {type: Number, default: 0},
            whatsapp: {type: Number, default: 0},
            trelocal: {type: Number, default: 0},
            tretitulo: {type: Number, default: 0},
            treregularidade:  {type: Number, default: 0},
            keras: {type: Number, default: 0},
            no_reply: {type: Number, default: 0},
            notreplied: {type: Number, default: 0}
    }, {collection: 'dashboard'});

module.exports = mongoose.model('dashboard', user);