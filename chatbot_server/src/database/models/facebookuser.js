const mongoose = require("mongoose"),

    user = new mongoose.Schema({
        _id: {type: String, trim: true, default:''},
        platform: {type: String, trim: true, default:''},
        senderid: {type: String, trim: true, default:''},
        page_access_token: {type: String, trim: true, default:''},
        facebookname: {type: String, trim: true, default:''},

        //TRE
        nome: {type: String, trim: true, default:''},
        titulodeeleitor: {type: String, trim: true, default:''},
        nomeMae: {type: String, trim: true, default:''},
        dtNascimento: {type: String, trim: true, default:''},
        situacaoeleitoral: {type: String, trim: true, default:''},
        numZona: {type: String, trim: true, default:''},
        municipio: {type: String, trim: true, default:''},
        bairro: {type: String, trim: true, default:''},
        enderecoZona: {type: String, trim: true, default:''},
        localVotacao: {type: String, trim: true, default:''},
        enderecoLocal: {type: String, trim: true, default:''},
        numSecao: {type: String, trim: true, default:''},
        //------

        datadenascimento: {type: String, trim: true, default:''},
        state: {type: String, trim: true, default:''},
        cpf: {type: String, trim: true, default:''},
        situacaoeleitoral: {type: String, trim: true, default:''},
        servicetime: {type: String, trim: true, default:''},
        chat: {
            messages: [],
            lastcalltime: { type: Date, default: Date.now() },
            tituloCalls: {type: Number, default: 0},
            localCalls: {type: Number, default: 0},
            regularidadeCalls: {type: Number, default: 0},
            calls: {type: Number, default: 0},
        },
        messagesubject: {type: Number, default: 0},
        chatsteps: {
            tretitulo: {type: Number, default: 0},
            treregularidade: {type: Number, default: 0},
            trelocal: {type: Number, default: 0},
            mesario: {type: Number, default: 0},
            turing: {type: Number, default: 0},
            chaterror: {type: Number, default: 0},
        },
        servicenotreplied: {type: Number, default: 0},
        goodjob: {type: Number, default: 0},
    }, {collection: 'facebook'});

module.exports = mongoose.model('facebook', user);
