const mongoose = require("mongoose"),

    knowledge = new mongoose.Schema({
        subject: {type: String, lowercase: true, trim: true, default: ''},
        inputs: {type: [String], lowercase: true, trim: true, default: []},
        key: {type: String, lowercase: true, trim: true, default: ''},
        output: {type: [String], trim: true, default: []},
        summary: {type: String, trim: true, default: ''},
        date: {type: Date, default: Date.now()},
        bot_function: {type: String, lowercase: true, trim: true, default: ''},
        insertedby: {type: String, trim: true, default: ''},
        calls: {type: Number, default: 0}
    }, {collection: 'knowledge'});

module.exports = mongoose.model('knowledge', knowledge);