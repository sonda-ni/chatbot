const
    mongooseFacebookUser = require("../../models/facebookuser"),
    mongooseAdminUser = require("../../models/administradores"),
    mongooseWebPageUser = require("../../models/webpageuser");

require('dotenv').config();

const webPage = (user, collection, PAGE_ACCESS_TOKEN) => {
    return mongooseWebPageUser.findById(user)
        .then(document => {
            if (document._doc) {
                return (document._doc);
            }
        })
        .catch(() => {
            let newUser = mongooseWebPageUser({
                _id: user,
                platform: collection,
                senderid: user,
                page_access_token: PAGE_ACCESS_TOKEN
            });
            newUser.save();
            return (newUser._doc);
        });
};
const admin = user => {
    return mongooseAdminUser.findById(user)
        .then(document => {
            if (document) {
                return(document._doc);
            }
        })
        .catch(err => {
            console.log(err);
        });
};

const facebook = (user, collection, PAGE_ACCESS_TOKEN) => {
    return mongooseFacebookUser.findById(user)
        .then(document => {
            if (document._doc) {
                return (document._doc);
            }
        })
        .catch(() => {
            let newUser = mongooseFacebookUser({
                _id: user,
                platform: collection,
                senderid: user,
                page_access_token: PAGE_ACCESS_TOKEN
            });
            newUser.save();
            return (newUser._doc);
        });
};
module.exports = (user = 0, collection ='', PAGE_ACCESS_TOKEN = '') => {
    return new Promise((resolve, reject) => {
        switch (collection) {
            case "facebook":
                resolve(facebook(user, collection, PAGE_ACCESS_TOKEN));
                break;
            case "webpage":
                resolve(webPage(user, collection, PAGE_ACCESS_TOKEN));
                break;
            case "admins":
                resolve(admin(user));
        }
    });
};
