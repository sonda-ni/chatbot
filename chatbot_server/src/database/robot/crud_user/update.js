const
    mongooseWebUser = require("../../models/webpageuser"),
    mongooseAdmin = require("../../models/administradores"),
    mongooseFacebookUser = require("../../models/facebookuser");

module.exports = mongooseUpdate = (data, collection, response_data = "") => {
    const filter = {_id: data.document._id};
    switch (collection) {
        case "facebook":
            data.document.goodjob++;
            data.document.chat.calls++;

            switch (response_data['bot_function']) {
                case "trelocal":
                    data.document.chat.localCalls++
                    break
                case "tretitulo":
                    data.document.chat.tituloCalls++
                    break
                case "treregularidade":
                    data.document.chat.regularidadeCalls++
                    break
            }

            data.document.chat.lastcalltime.setDate(new Date().getDate());
            data.document.chat.messages.push({"bot": response_data['message'] || data.output,
                "time": Date.now(), "service": data.service});
            //-------------
            mongooseFacebookUser.updateOne(filter, data.document)
                .then(result => {
                    //console.log(result)
                })
                .catch(err => {
                    //console.log(err)
                });
            break;
        case "webpage":
            //Before update
            data.document.goodjob++;
            data.document.chat.calls++;
            data.document.chat.lastcalltime.setDate(new Date().getDate());
            data.document.chat.messages.push({"bot": response_data['message'] || data.output,
                "time": Date.now(), "service": data.service});
            //-------------
            mongooseWebUser.updateOne(filter, data.document)
                .then(result => {
                    //console.log(result)
                })
                .catch(err => {
                    //console.log(err)
                });
            break;
        case 'admins':
            mongooseAdmin.updateOne(filter, data.document)
                .then(result => {
                    //console.log(result)
                })
                .catch(err => {
                    //console.log(err)
                });
            break;
    }
};
