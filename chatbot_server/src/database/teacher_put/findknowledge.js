const
    knowledge = require('../../database/models/knowledge'),
    path = require('../../services/dice_coeficiente/learning'),
    normalizer = require('../../services/dice_coeficiente/string-analytics/regularizeSentence'),
    card = require('../../public/view/modules/card');


searchSubjects = async () => {
    let cardsList = '';
    let list = [];
    //console.log("Search");
    return await knowledge.find({}, {subject:1},
        function (err, list) {
            if (err) return "Not Loaded" + err;
        }
    )
        .sort({ calls: -1 })
        .limit(20)
        .then((result) => {return result});
};


searchKnowledgeByValue = async (search) => {
    let cardsList = '';
    let knowledgeList = [];
    //console.log("Search");
    return await knowledge.find(
        {"subject": {"$regex": search, "$options": "i"}}
    )
        .sort({ calls: -1 })
        .limit(20)
        .then((list) => {
            list.forEach(knowledge => {
                knowledgeList.push(knowledge._doc);
                cardsList += card(knowledge._doc);
            });
            return {'cardsList': cardsList, 'list': knowledgeList}
        })
        .catch(err => {return err});
};
searchKnowledgeAll = async () => {
    let cardsList = '';
    let cardsArray = [];
    await knowledge.find()
        .sort({ calls: -1 })
        .limit(20)
        .then(list => {
            //console.log("Loaded All");
            list.forEach(knowledge => {
                cardsArray.push(knowledge._doc);
                cardsList += card(knowledge);
            });
        })
        .catch(e => {
            return "Not Loaded" + e
        });
    return {'cardsList': cardsList, 'list': cardsArray}
};

knowledgeInsert = async (req) => {
    try {
        if (req.body.inputs || req.body.output || req.body.subject) {
            let input = {};
            let regularInputs = [];
            req.body.inputs.forEach(message => {
                regularInputs.push(normalizer(message));
            });
            input['subject'] = req.body.subject;
            input['inputs'] = regularInputs;
            input['key'] = normalizer(req.body.key);
            input['output'] = req.body.output;
            input['summary'] = req.body.summary;
            input['bot_function'] = req.body.bot_function;
            input['user'] = req.body.insertedby || input['user'] || 'Not defined';
            return await path.insertKnowledge(input)
                .then(obj => {return obj})
                .catch(err => {return err});
        }
    } catch (e) {
        return ('Form is not correct ---> ' + e);
    }
};
    module.exports = {
        searchKnowledgeByValue,
        searchKnowledgeAll,
        knowledgeInsert,
        searchSubjects
    };