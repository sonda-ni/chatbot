const
    admins = require('../../models/administradores'),
    bcrypt = require('bcrypt');

module.exports = insertAdmin = async (adminDocument) => {
    return admins.findById(adminDocument.email).then(async document => {
        if (document){
            return('User Exists');
        }else{
            let salt = await bcrypt.genSalt(8);
            let newUser = admins({
                _id: adminDocument.email,
                firstName:  adminDocument.firstName,
                lastName:  adminDocument.lastName,
                email:  adminDocument.email,
                password: await bcrypt.hash(adminDocument.password, salt),
                business:  adminDocument.business,
                access: adminDocument.access
            });
            newUser.save();
            return("Admin Saved");
        }
    }).catch((e) => {
        return('Verify Parameters: '+ e);
    });
};

