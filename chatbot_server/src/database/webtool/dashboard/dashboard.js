module.exports = insert = (senderID, PAGE_ACCESS_TOKEN, TABLE) => {
    console.log(TABLE);
    return new Promise((resolve, reject) => {

        let
            dbo = db.db("robot"),
            dashboard = {
                facebookInteraction: 0,
                websiteInteraction: 0,
                interationQuantity: 0,
                tituloQuestions: 0,
                localQuestions: 0,
                regularidadeQuestions: 0,
                chatTotalCalls: 0,
                ageAverage: 0,
                numberOfUsers: 0,
                zonas: {
                    ac: 0,
                    al: 0,
                    am: 0,
                    ap: 0,
                    ba: 0,
                    ce: 0,
                    df: 0,
                    es: 0,
                    go: 0,
                    ma: 0,
                    mg: 0,
                    ms: 0,
                    mt: 0,
                    pa: 0,
                    pb: 0,
                    pe: 0,
                    pi: 0,
                    pr: 0,
                    rj: 0,
                    rn: 0,
                    ro: 0,
                    rr: 0,
                    rs: 0,
                    sc: 0,
                    se: 0,
                    sp: 0,
                    to: 0,
                    zz: 0
                }
            };
        dbo.collection(TABLE).insertOne(dashboard, function (err, res) {
            if (err) reject(err);
            console.log("1 document inserted");
            resolve(res)
        });
    });
};

