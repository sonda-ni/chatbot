module.exports = dashboardData = (userName) => {
    return {
        //Dados Usuarios Logado
        name: userName,
        //Dados Quantidade de  Usuário
        totalUsuario: "350",
        porcentUsuario: "4%",
        //Dados Atendimento
        tempoAtend: "30Min",
        porcentAtend: "3%",
        //Dados Taxa de Sucesso
        taxaSucesso: "86%",
        porcentSucesso: "95%",
        //Dados Fe
        totalFe: "4,6",
        percentFe: "12%",

        //Zonas Eleitorias
        Zona1: "São Paulo",
        percentzona1: "62",
        Zona2: "Brasília",
        percentzona2: "35",
        Zona3: "Rio de Janeiro",
        percentzona3: "15",
        Zona4: "Salvador",
        percentzona4: "12",

        //Mais Buscadas
        maisBuscadas1: "Título de Eleitor",
        percentmaisBuscadas1: "41%",
        maisBuscadas2: "Identidade",
        percentmaisBuscadas2: "33%",
        maisBuscadas3: "CPF",
        percentmaisBuscadas3: "16%",
        maisBuscadas4: "Cartão",
        percentmaisBuscadas4: "7%",

        server: process.env.LOCALSERVER
    }
};