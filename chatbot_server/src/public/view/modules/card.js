module.exports = (card) => {
    let cardString = {};
    cardString['subject'] = card.subject;
    cardString['inputs'] = card.inputs;
    cardString['output'] = card.output;
    cardString['date'] = card.date;
    cardString['key'] = card.key;
    cardString['calls'] = card.calls;
    cardString['bot_function'] = card.bot_function || "";
    cardString['insertedby'] = card.insertedby;
    cardString['summary'] = card.summary;
    let jsonCard = JSON.stringify(cardString.toString());

return `
<div class="card shadow" id="cardIn" style="width: 18rem;">
    <div class="card-body">
        <h5 class="card-title ">Assunto</h5>
        <h6 class="card-subtitle mb-2 text-muted">${card.subject}</h6>
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    Perguntas feitas: ${cardString['calls']}
                </div>
                <div class="carousel-item">
                    <a class="card-link text-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Inputs: ${card.inputs.length}</a>
                </div>
                <div class="carousel-item">
                    <a class="card-link text-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Outputs: ${card.output.length}</a>
                </div>
            </div>
        </div>


        <p class="card-text">${card.summary}</p>
        <button type="button" class="btn btn-default btn-sm update" id=${card._id.toString()} >Verificar</button>
        <button type="button" class="btn btn-default btn-sm deleteIcon" id=${card._id.toString()} >Deletar</button>
    </div>
</div>
`
}
;