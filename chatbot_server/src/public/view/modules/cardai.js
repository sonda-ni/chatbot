module.exports = (doc) => {
return `
<div class="card shadow" id="cardIn" style="width: 18rem;">
    <div class="card-header">
        Nível de acerto para esta entrada: ${doc.finalrating}<br>
        Input encontrado: ${doc.base.bestmatchknowledge}
    </div>
    <div class="card-body">
        <h5 class="card-title ">Assunto</h5>
        <h6 class="card-subtitle mb-2 text-muted">${doc.base.knowledge.subject}</h6>
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    Perguntas feitas: ${doc.base.knowledge.calls}
                </div>
                <div class="carousel-item">
                    <a class="card-link text-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Inputs: ${doc.base.knowledge.inputs.length}</a>
                </div>
                <div class="carousel-item">
                    <a class="card-link text-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Outputs: ${doc.base.knowledge.output.length}</a>
                </div>
            </div>
        </div>

        <p class="card-text">${doc.base.knowledge.summary}</p>
    </div>
</div>
`};