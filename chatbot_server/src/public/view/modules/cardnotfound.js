module.exports = () => {
    return `
<div class="card shadow">
  <div class="card-body">
    <h5 class="card-title">Nenhum conhecimento encontrado</h5>
    <p class="card-text">Escreva mais ou de outra forma. <br>
     Você pode ensinar o ROBOT se preferir!</p>
    <a href="machinelearning" class="btn btn-primary">Ensinar</a>
  </div>
</div>
`
};