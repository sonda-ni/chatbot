
const
    //bodyParser = require('body-parser'),
    crypto = require('crypto'),
    https = require('https'),
    dashboardData = require('../../database/models/dashboard'),
    gateWay = require('../../services/gateway/gateway');
    //express = require('express');
    let router = require('express').Router();


// App Secret pode ser recuperado em App Dashboard no Facebook.
const APP_SECRET = process.env.APP_SECRET || '';

// Valor arbitrário para validar o webhook
const VALIDATION_TOKEN = process.env.VALIDATION_TOKEN || '';

// Gere o seu page access token pela página no Facebook do App Dashboard
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || '';

// URL where the app is running (include protocol). Used to point to scripts and
// assets located at this address.
const SERVER_URL = process.env.LOCALSERVER || '';

if (!(APP_SECRET && VALIDATION_TOKEN && PAGE_ACCESS_TOKEN && SERVER_URL)) {
    console.error("Missing config values");
    process.exit(1);
}

/*
 * Use your own validation token. Check that the token used in the Webhook
 * setup is the same token used here.
 *
 */
router.get('/', function (req, res) {
    if (req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === VALIDATION_TOKEN) {
        console.log("Sonda -> Validating webhook");
        res.status(200).send(req.query['hub.challenge']);
    } else {
        console.error("Failed validation. Make sure the validation tokens match. Verify if " +
            "the validationToken in .env is the same!");
        res.sendStatus(403);
    }
});




/*
 * All callbacks for Messenger are POST-ed. They will be sent to the same
 * webhook. Be sure to subscribe your app to your page to receive callbacks
 * for your page.
 * https://developers.facebook.com/docs/messenger-platform/product-overview/setup#subscribe_app
 *
 */
router.post('/', function (req, res) {
    let data = req.body;

    // Make sure this is a page subscription
    if (data.object == 'page') {
        // Iterate over each entry
        // There may be multiple if batched
        data.entry.forEach(function (pageEntry) {

            // Iterate over each messaging event
            pageEntry.messaging.forEach(function (messagingEvent) {
                if (messagingEvent.optin) {
                    receivedAuthentication(messagingEvent);
                } else if (messagingEvent.message) {
                    receivedMessage(messagingEvent)
                        .then(status => {
                            res.sendStatus(status);
                        })
                        .catch(err => {
                            console.log(err);
                            res.sendStatus(200)})
                } else {
                    console.log("Webhook received unknown messagingEvent: ", messagingEvent);
                }
            });
        });
        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know you've
        // successfully received the callback. Otherwise, the request will time out.
    }
});

/*
 * This path is used for account linking. The account linking call-to-action
 * (sendAccountLinking) is pointed to this URL.
 *
 */


/*
 * Verify that the callback came from Facebook. Using the App Secret from
 * the App Dashboard, we can verify the signature that is sent with each
 * callback in the x-hub-signature field, located in the header.
 *
 * https://developers.facebook.com/docs/graph-api/webhooks#setup
 *
 */
function verifyRequestSignature(req, res, buf) {
    let signature = req.headers["x-hub-signature"];

    if (!signature) {
        // For testing, let's log an error. In production, you should throw an
        // error.
        console.error("Couldn't validate the signature.");
    } else {
        let elements = signature.split('=');
        let method = elements[0];
        let signatureHash = elements[1];

        let expectedHash = crypto.createHmac('sha1', APP_SECRET)
            .update(buf)
            .digest('hex');

        if (signatureHash != expectedHash) {
            throw new Error("Couldn't validate the request signature.");
        }
    }
}

/*
 * Authorization Event
 *
 * The value for 'optin.ref' is defined in the entry point. For the "Send to
 * Messenger" plugin, it is the 'data-ref' field. Read more at
 * https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
 *
 */
function receivedAuthentication(event) {
    let senderID = event.sender.id;
    let recipientID = event.recipient.id;
    let timeOfAuth = event.timestamp;

    // The 'ref' field is set in the 'Send to Messenger' plugin, in the 'data-ref'
    // The developer can set this to an arbitrary value to associate the
    // authentication callback with the 'Send to Messenger' click event. This is
    // a way to do account linking when the user clicks the 'Send to Messenger'
    // plugin.
    let passThroughParam = event.optin.ref;

    // console.log("Received authentication for user %d and page %d with pass " +
    //     "through param '%s' at %d", senderID, recipientID, passThroughParam,
    //     timeOfAuth);

    // When an authentication is received, we'll send a message back to the sender
    // to let them know it was successful.
    //sendTextMessage(senderID, "Authentication successful");
}

/*
 * Message Event
 *
 * This event is called when a message is sent to your page. The 'message'
 * object format can vary depending on the kind of message that was received.
 *
 * For this example, we're going to echo any text that we get. If we get some
 * special keywords ('button', 'generic', 'receipt'), then we'll send back
 * examples of those bubbles to illustrate the special message bubbles we've
 * created. If we receive a message with an attachment (image, video, audio),
 * then we'll simply confirm that we've received the attachment.
 *
 */
const receivedMessage = async (event) => {
    return new Promise(async (resolve, reject) => {
        let senderID = event.sender.id;
        let message = event.message;
        let name = {};
        dashboardData.findOneAndUpdate({_id: 1}, { $inc: { facebook: 1 }})
            .then(() => {})
            .catch(() => {});

        return https.get(`https://graph.facebook.com/${senderID}?fields=first_name,last_name,profile_pic&access_token=EAAjdbsWZBTvkBAEgZC1xB6vwSa5kaGhQTufqLxYu9DoK4ptNliGdR0AfkSrDyZC8o8c82vwxNx1JBBnEqYSpkBk3Bo82QePgT4xxIRlaQZBVMOTRZAG7TrSCZAZCMGO2wCkl2WPL6D8fqeRLGAy78LdokZCk2My6ZAhZAPZCi85LCZA48gZDZD`, resp => {
            let data = "";

            resp.on("data", chunk => {
                data += chunk;
            });

            return resp.on("end", async () => {
                name = JSON.parse(data) || 'Not authorized by user';


// console.log("Received message for user %d and page %d at %d with message:",
                //     senderID, recipientID, timeOfMessage);
                // console.log(JSON.stringify(message));

                let isEcho = message.is_echo;

                // You may get a text or attachment but not both
                let messageText = message.text;
                //let messageAttachments = message.attachments;
                let quickReply = message.quick_reply;

                if (isEcho) {
                    // Just logging message echoes to console
                    // console.log("Received echo for message %s and app %d with metadata %s",
                    //     messageId, appId, metadata);
                    return;
                } else if (quickReply) {
                    let quickReplyPayload = quickReply.payload;
                    // console.log("Quick reply for message %s with payload %s",
                    //     messageId, quickReplyPayload);

                    //sendTextMessage(senderID, "Quick reply tapped");
                    return;
                }
                let input = {};
                input['PAGE_ACCESS_TOKEN'] = PAGE_ACCESS_TOKEN;
                input['collection'] = "facebook";
                input['message'] = messageText;
                input['user'] = senderID.toString();
                input['test'] = event.test;
                input['name'] = name.first_name;
                await gateWay.gateway_flow(input)
                    .then((res) => {resolve(res) })
                    .catch((err) => {reject(err) })//)  send message to the bot
            })

        });




    });

};

module.exports = router;