const request = require('request');

module.exports = (user, message, PAGE_ACCESS_TOKEN) => {
    //FaceBook JSON OUT
    let messageDataFacebookFormatted = {
        recipient: {id: user},
        message: {text: message}
    };

    request({
        uri: 'https://graph.facebook.com/v2.6/me/messages',
        qs: {access_token: PAGE_ACCESS_TOKEN},
        method: 'POST',
        json: messageDataFacebookFormatted
    }, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            let recipientId = body.recipient_id;
            let messageId = body.message_id;
            if (messageId) {
                //console.log("Successfully sent message with id %s to recipient %s",
                    //messageId, recipientId);
            } else {
                //console.log("Successfully called Send API for recipient %s",
                    //recipientId);
            }
        } else {
            //console.error("Failed calling Send API", response.statusCode, response.statusMessage, body.error);
        }
    });
};