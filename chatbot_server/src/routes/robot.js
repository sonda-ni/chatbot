const
    gateWay = require('../services/gateway/gateway'),//eval
    AuthMiddleware = require('../middleware/authtoken'),
    teacher = require('./teacher/teacher');
let router = require('express').Router();

//Robot WebTool
const MIDDLEWARE = [AuthMiddleware];


const getMessage = async(req, res, whatToDo) => {
    if (req.body.message){
        if (req.body.user) {
            let input = {};
            input['message'] = req.body.message;
            input['user'] = req.body.user;
            input['PAGE_ACCESS_TOKEN'] = req.body.PAGE_ACCESS_TOKEN || 'Not necessary.';
            input['collection'] = "webpage";
            input['user'] = req.body.user || 'Not defined';
            //let message = await gateWay.hippoCampus(req.body.user, req.body.message);
            return await eval(`gateWay.${whatToDo}(input);`);
        } else {
            res.json({
                "message":'Ops... Quem é você?'
            });
        }
    }else{
        res.json({
            "message":'Ops... Escreva algo !'
        });
    }

};

router.post('/', async function (req, res) {
    res.send(await getMessage(req, res, 'gateway_flow'));
});

router.post('/data', MIDDLEWARE,  async function (req, res) {
    res.json(await getMessage(req, res, 'gateway_flow'));
});

router.use('/', teacher);
module.exports = router;