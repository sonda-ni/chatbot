const
    facebook = require('./facebook/facebook'),
    authorize = require('./facebook/authorize'),
    webTool = require('./webtool/webtool'),
    robot = require('./robot'),
    teacher = require('./teacher/teacher'),
    login = require('../middleware/login'),
    loginOut = require('../middleware/loginout');
    let router = require('express').Router();

//Login

router.get('', (req, res) => {
    res.status(200).render('index', {
        server: process.env.LOCALSERVER
    });
});

router.post('/login', login);
router.get('/logout', loginOut);


//Routes
router.use('/facebook', facebook);
router.use('/authorize', authorize);
router.use('/webtool', webTool);
router.use('/robot', robot);
router.use('/teacher', teacher);


module.exports = router;