const
    findKnowledge = require('../../database/teacher_put/findknowledge'),
    knowledge = require('../../database/models/knowledge'),
    path = require('../../services/dice_coeficiente'),
    cards = require('../../public/view/modules/cardai'),
    normalizer = require('../../services/dice_coeficiente/string-analytics/regularizeSentence'),
    cardNotFound = require('../../public/view/modules/cardnotfound'),
    call_machine_learning = require('../../services/gateway/call_python/call_machine_learning'),
    AuthMiddleware = require('../../middleware/authtoken');
let router = require('express').Router();


//Robot WebTool
const MIDDLEWARE = [AuthMiddleware];
router.use(MIDDLEWARE);

//delete ------------------------ falta

//Subject research
router.search('/knowledge', async function (req, res) {
    let result;
    if (req.body.search) {
        result = await findKnowledge.searchKnowledgeByValue(req.body.search);
    } else {
        result = await findKnowledge.searchKnowledgeAll();
    }
    if (result.cardsList)
        res.status(200).json({cards: result.cardsList, list: result.list});
    else
        res.status(404).json({list: result});
});
router.search('/knowledgesubject', async function (req, res) {
    await findKnowledge.searchSubjects().then((result) => res.json({'list': result}))
});

// A.I. Insert knowledge
router.put('/knowledgeia', async (req, res) => {
    if (req.body.subject && req.body.inputs && req.body.key && req.body.output && req.body.summary) {
        let obj = await findKnowledge.knowledgeInsert(req)
        if (obj.status === 200) {
            let data_in = await call_python()
            res.status(200).json({
                "mongodb_message": obj.message,
                "mongo_status": obj.status,
                "keras": data_in.data
            });
        } else {
            res.status(200).json({
                "mongodb_message": obj.message,
                "mongo_status": 300,
                "keras": "No Data"
            })
        }

    } else {
        res.status(200).json({
            "mongodb_message": "Formulário não preenchido.",
            "mongo_status": 300,
            "keras": "No Data"
        })
    }
});

router.post('/knowledgeia', async (req, res) => {
    let inp = [];
    let outs = [];
    if (req.body.inputs && req.body.output) {
        req.body.inputs.forEach(value => {
            inp.push(normalizer(value))
        })
        req.body.output.forEach(value => {
            outs.push(normalizer(value))
        })

        knowledge.updateOne({'_id': req.body.id.toLowerCase()}, {
            subject: normalizer(req.body.subject),
            inputs: inp,
            output: outs,
            summary: normalizer(req.body.summary)
        })
            .then(result => {
                call_python()
                res.send("Conhecimento Atualizado !")
            })
            .catch(result => {
                res.send("Erro")
            })
    }
});

router.delete('/knowledgeia', async (req, res) => {
    try {
        knowledge.deleteOne({_id: req.body.id}, function (err) {
            if (err) res.send("Formulário não deletado. Verificar LOG.");
        });
    } catch (e) {
        res.send("Formulário não deletado. Verificar LOG.")
    }
    res.send("Conhecimento Deletado!")
});

// Train Model
router.get('/train', async (req, res) => {
    res.json(await call_python())
});


//IA knowledge research
router.search('/knowledgeia', async (req, res) => {
    let knowledgeList = await path.knowledgeView(req.body.search);
    let number = 0.6;
    let knowledgeListFiltered = knowledgeList.filter(math => math.finalrating >= number);
    let cardsList = [];
    if (knowledgeListFiltered.length > 0) {
        knowledgeListFiltered.forEach(bestMatches => {
            cardsList += cards(bestMatches);
        });
        res.json({cards: cardsList, list: knowledgeListFiltered})
    } else {
        let obj = {cards: cardNotFound()};
        res.json(obj);
    }

});

let call_python = async () => {
    return await call_machine_learning.train_machine_learning()
        .then(data => {
            return {
                data: data
            }
        })
}


module.exports = router;