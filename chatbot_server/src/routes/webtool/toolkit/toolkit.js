const
    router = require('express').Router(),
    admins = require('../toolkit/tools/admintool'),
    teacher = require('../../teacher/teacher'),
    users = require('../toolkit/tools/userstool');


//Tool Routes
router.use('', admins);
router.use('', teacher);
router.use('', users);


module.exports = router;