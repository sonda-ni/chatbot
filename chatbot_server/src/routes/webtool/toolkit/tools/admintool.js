const
    cors = require('cors'),
    admins = require('../../../../database/models/administradores'),
    corsOptionsDelegate = require('../../../../middleware/cors'),
    insertAdmin = require('../../../../database/webtool/admins/createadmin'),
    bcrypt = require('bcrypt');
let router = require('express').Router();


router.search("/admins",cors(corsOptionsDelegate), async(req, res) => {
    try {
        await admins.find().then((list) => {
            res.status(200).json({list:list})
        });
    }catch (e) {
        res.status(200).json({response:'Error'})
    }

});
//update
router.merge("/admins",cors(corsOptionsDelegate), async(req, res) => {
    let updateDoc;
    try {
        let salt = await bcrypt.genSalt(8);
        if (req.body.password === ""){
            //Can be "admin" or "user".
            updateDoc = {
                firstName:  req.body.firstName,
                lastName:  req.body.lastName,
                business:  req.body.business,
                access:  req.body.access
            }
        }else{
            updateDoc = {
                firstName:  req.body.firstName,
                lastName:  req.body.lastName,
                password: await bcrypt.hash(req.body.password, salt),
                business:  req.body.business,
                access:  req.body.access
            }
        }
        const filter = {_id: req.body.email};
        await admins.updateOne(filter, updateDoc)
            .then(result => {
                res.status(200).json({"response": "Updated"})
            })
            .catch(err => {
                res.status(200).json({"response": "Updated Failed"})
            });
    }catch (e) {
        res.status(200).json({"response":'Error'})
    }

});
router.delete("/admins",cors(corsOptionsDelegate), async(req, res) => {
    try {
        await admins.findByIdAndDelete(req.body.email).then((result) => {
            if (result.errors) res.status(200).json({response:'Error'});
            if (result._doc) res.status(200).json({response:'Admin Deleted'});
        })
    }catch (e) {
        res.status(200).json({response:'Error'})
    }

});
router.subscribe("/admins",cors(corsOptionsDelegate), async(req, res) => {
    if (req.body.email && req.body.password && req.body.firstName
        && req.body.lastName && req.body.business && req.body.access){
        try {
            insertAdmin({
                _id: req.body.email,
                firstName:  req.body.firstName,
                lastName:  req.body.lastName,
                email: req.body.email,
                password:  req.body.password,
                business:  req.body.business,
                access: req.body.access
            }).then(result => {
                res.status(200).json({response: result.toString()});
            }).catch(e => {
                res.status(200).json({response: e.toString()});
            });
        }catch (e) {
            res.status(200).json({response:'Error'})
        }
    }else{
        res.status(200).json({response:'Form not fulfilled'})
    }


});

module.exports = router;