const webpage = require('../../../../database/models/webpageuser');
const facebook = require('../../../../database/models/facebookuser');
let router = require('express').Router();


// GET - Shop Product Page | - Displaying demanded product page with page numbers
router.search('/webpage/users', async (req, res, next) => {
// Declaring variable


    let numOfProducts = 0;

    let regString = req.body.platform !== 'facebook'? req.body.identifier.toString(): req.body.identifier;


    try {
        //numOfProducts = await webpageUser.estimatedDocumentCount({ sort: { 'created_at' : -1 }});
        // Find Demanded Products - Skipping page values, limit results       per page

        eval(`${req.body.platform}`).find({
            $and: [
                {$or: [{"_id": {"$regex": regString, "$options": 'i'}}]},
                {$or: [{'titulodeeleitor': req.body.tittle}]}
            ]
        })
            .sort({_id: -1})
            .limit(20)
            .exec(function (err, docs) {
                if (err) console.log(err);
                res.json({
                    foundUsers: docs,
                    numOfResults: numOfProducts
                });
            })


    } catch (e) {
        res.status(200).json({response: 'Error'})
    }


});
// Exporting Shop Router
module.exports = router;