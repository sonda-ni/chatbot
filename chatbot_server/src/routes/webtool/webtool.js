const
    AuthMiddleware = require('../../middleware/authtoken'),
    dashboardData = require('../../jobs/dashboard/dashboardlogic'),
    mathAnalysis = require('../../services/dice_coeficiente/mathanalysis/dashmath'),
    toolKit = require('./toolkit/toolkit');
let router = require('express').Router();

//Robot WebTool
const MIDDLEWARE = [AuthMiddleware];

router.use(MIDDLEWARE);
router.use('/toolkit',toolKit);

router.get('/dashboard', async (req, res) => {
    res.status(200).render('dashboard', {
        dashboard: await mathAnalysis.updatePage(),
        nav: 'Dashboard',
        server: process.env.LOCALSERVER});
});
router.get('/newadmin', (req, res) => {
    res.status(200).render('newadmin', {
        name: req['userName'],
        nav: 'Criar Admin',
        server: process.env.LOCALSERVER
    });
});
router.get('/users', (req, res) => {
    res.status(200).render('users', {
        name: req['userName'],
        nav: 'Procurar por Usuários',
        server: process.env.LOCALSERVER
    });
});
router.get('/machinelearning', (req, res) => {
    res.status(200).render('machinelearning', {
        name: req['userName'],
        LEARNING_RATE: process.env.LEARNING_RATE,
        EPOCHS: process.env.EPOCHS,
        MESSENGER_PRECISION: process.env.MESSENGER_PRECISION,
        LEARNING_PRECISION: process.env.LEARNING_PRECISION,
        FIND_KNOWLEDGE_PRECISION: process.env.FIND_KNOWLEDGE_PRECISION,
        nav: 'Supervised Machine Learning',
        server: process.env.LOCALSERVER
    });
});
router.get('/knowledge', (req, res) => {
    res.status(200).render('knowledge', {
        name: req['userName'],
        nav: 'Conhecimentos',
        server: process.env.LOCALSERVER
    });
});
router.get('/admins', (req, res) => {
    res.status(200).render('admins', {
        name: req['userName'],
        nav: 'Users',
        server: process.env.LOCALSERVER
    });
});

module.exports = router;