/* jshint src: true, devel: true */
'use strict';

const app = require('./app');
require('./database/connect');

// Server
app.listen(app.get('port'), function () {
    console.log('****** Server running on port', app.get('port')+ ' ******');
});