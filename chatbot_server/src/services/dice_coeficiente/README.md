string-analytics
=================

Finds degree of similarity between two strings,
 based on [Dice's Coefficient](http://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient),
  which is mostly better than [Levenshtein distance](http://en.wikipedia.org/wiki/Levenshtein_distance).

In your code:

```javascript
const stringAnalytics = require('string-similarity');

const similarity = stringAnalytics.compareTwoStrings('healed', 'sealed'); 

const matches = stringAnalytics.findBestMatch('healed', ['edward', 'sealed', 'theatre']);
```

  
-> compareTwoStrings( String , String )

```javascript
stringAnalytics.compareTwoStrings('healed', 'sealed');
// → 0.8

stringSimilarity.compareTwoStrings('Olive-green table for sale, in extremely good condition.', 
  'For sale: table in very good  condition, olive green in colour.');
// → 0.6060606060606061

```

### findBestMatch(mainString, targetStrings)

-> findBestMatch( String , Array)

```javascript
stringSimilarity.findBestMatch('Olive-green table for sale, in extremely good condition.', 
[
  'For sale: green Subaru Impreza, 210,000 miles', 
  'For sale: table in very good condition, olive green in colour.', 
  'Wanted: mountain bike with at least 21 gears.'
]);

// → 
{ ratings:
   [ { target: 'For sale: green Subaru Impreza, 210,000 miles',
       rating: 0.2558139534883721 },
     { target: 'For sale: table in very good condition, olive green in colour.',
       rating: 0.6060606060606061 },
     { target: 'Wanted: mountain bike with at least 21 gears.',
       rating: 0.1411764705882353 } ],
  bestMatch:
   { target: 'For sale: table in very good condition, olive green in colour.',
     rating: 0.6060606060606061 },
  bestMatchIndex: 1 
}
```