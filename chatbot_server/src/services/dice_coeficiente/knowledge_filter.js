const
    stringAnalytics = require('./string-analytics/src'),
    learning = require('./learning'),
    knowledge = require('../../database/models/knowledge');


const dataBase = async () => {
    let docs = [];
    return await knowledge.find()
        .then(list => {
            list.forEach(doc => {
                docs.push(doc._doc)
            });
            return docs;
        })
        .catch(e => {
            return e
        })
};

const get_dataBase = async () => {
    let docs = [];
    return await knowledge.find()
        .then(list => {
            list.forEach(doc => {
                docs.push(doc._doc)
            });
            return docs;
        })
        .catch(e => {
            return e
        })
};


const analytics = async () => {
    let analytics = [];
    let bestChart = [];
    await this['database'].forEach(knowledge => {
        let parameters = stringAnalytics.findBestMatch(this.message, knowledge['inputs']);
        analytics.push(
            {
                'inputsrating': parameters.bestMatch.rating,
                'bestmatchknowledge': parameters.bestMatch.target,
                'knowledge': knowledge
            })
    });
    let number = Number(process.env.MESSENGER_PRECISION);
    let x = analytics.filter(math => math.inputsrating >= number);
    analytics.forEach(doc => {
        bestChart.push({
            'keysrating': stringAnalytics.compareTwoStrings(this.message, doc.knowledge.key),
            'base': doc
        });
    });
    // part 2
    if (x || bestChart) {
        bestChart.forEach(chart => {
            let average = (chart.keysrating + chart.base.inputsrating) / 2;
            if (average < chart.keysrating || average < chart.base.inputsrating) {
                chart['finalrating'] = chart.keysrating < chart.base.inputsrating ?
                    chart.base.inputsrating : chart.keysrating;
            } else {
                chart['finalrating'] = average;
            }
        })
    }
    return bestChart.sort(function (a, b) {
        return b.finalrating - a.finalrating
    })
};

const analyticsCortex = async () => {
    //IF the input test is equal or almost the same
    let testMath = this['math']
        .filter(knowledge => {
            return knowledge.finalrating >= 0.94
        })
        .map(function (obj) {
            let num = Math.floor(Math.random() * obj.base.knowledge.output.length);
            return {
                recognized: true,
                data: {
                    matchPercentage: obj,
                    output: obj.base.knowledge.output[num],
                    id: obj.base.knowledge._id.toString(),
                }
            };
        });
    if (testMath.length === 1) return testMath[0];
    //--------------------------------------------

    let bestChoice = this['math'][0];
    if (bestChoice.finalrating >= process.env.MESSENGER_PRECISION) {
        let num = Math.floor(Math.random() * bestChoice.base.knowledge.output.length);
        return {
            recognized: true,
            data: {
                matchPercentage: bestChoice,
                output: bestChoice.base.knowledge.output[num],
                id: bestChoice.base.knowledge._id.toString(),
            }
        }
    } else {
        return {
            recognized: false,
            data: {
                matchPercentage: this['math'][0],
            }

        };
    }

};

const newKnowledgeInsert = async (input) => {
    return await learning.insertKnowledge(input);
};

const verifyKnowledge = async (input) => {
    return await learning.verifyKnowledge(input);
};

//Cortex flow to answer each message percentage
const cortexResponse = async (message, input = null) => {
    if (input) this['input'] = input;
    this['message'] = message;
    this['database'] = await dataBase();
    this['math'] = await analytics();
    return await analyticsCortex();
};

//Knowledge View Flow
const knowledgeView = async (message) => {
    this['message'] = message;
    this['database'] = await dataBase();
    return await analytics();
};

module.exports = {
    cortexResponse,
    knowledgeView,
    verifyKnowledge,
    get_dataBase,
    newKnowledgeInsert
};