const
    stringAnalytics = require('./string-analytics/src'),
    learning = require('./learning'),
    knowledge = require('../../database/models/knowledge');


const isEmpty = (obj) => {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

const dataBase = async () => {
    return await knowledge.find()
        .then(doc => {
            return doc
        }).catch(e => {
            return e
        })
};

const analytics = async (message) => {
    let analytics = [];
    await this['database'].forEach(knowledge => {
        let parameters = stringAnalytics.findBestMatch(message, knowledge._doc['inputs']);
        analytics.push(
            {
                'bestmatchknowledge': parameters.bestMatch.target, 'rating': parameters.bestMatch.rating,
                'knowledge': knowledge
            })
    });
    let base = analytics.sort(function (a, b) {
        return a.rating - b.rating
    });
    return base.filter(knowledge => knowledge.rating >= process.env.LEARNING_PRECISION);
};

//Insert Knowledge If Not Exists
const knowledgeInsert = async () => {
    if (this['database'].length === 0) {
        let newKnowledge = knowledge({
            subject: this.input['subject'].trim(),
            inputs: this.input['inputs'],
            key: this.input['key'].trim(),
            output: this.input['output'],
            insertedby: this.input['user'],
            summary: this.input['summary'],
            bot_function: this.input['bot_function']
        });
        await newKnowledge.save();
        return 'Conhecimento Salvo...'
    } else {
        try {
            let mathData = [];
            for (let i = 0; i < this.input['inputs'].length; i++) {
                let data = await analytics(this.input['inputs'][i]);
                mathData.push(data[0]);
            }
            let test = false;
            for (let i = 0; i < mathData.length; i++) {
                if (mathData[i] !== undefined && mathData[i].rating >= process.env.LEARNING_PRECISION)
                    test = true;

            }
            if (test) {
                return {
                    status: 300,
                    message:`Robot diz: Verifique os conhecimentos acumulados, existe algo similar. Procentagem de corte: 
                ${process.env.LEARNING_PRECISION}`
                };
            } else {
                let newKnowledge = knowledge({
                    subject: this.input['subject'],
                    inputs: this.input['inputs'],
                    key: this.input['key'],
                    output: this.input['output'],
                    insertedby: this.input['insertedby'],
                    summary: this.input['summary'],
                    bot_function: this.input['bot_function']
                });
                await newKnowledge.save();
                return {
                    status: 200,
                    message:`Conhecimento Salvo... Nenhum conhecimento parecido encontrado! Procentagem de corte: 
                ${process.env.LEARNING_PRECISION}`
                }
            }
        } catch (e) {
            return {
                status: 300,
                message:`Error: ${e}`
            }
        }
    }
};
const insertKnowledge = async (input) => {
    if (input) this['input'] = input;
    this['database'] = await dataBase();
    return await knowledgeInsert();
};

const verifyKnowledge = async (input) => {
    if (input) this['input'] = input;
    this['database'] = await dataBase();
    this['didMath'] = await analytics(input['message']);
    let mathData = [];
    await this['didMath'].forEach(message => {
        this['message'] = message;
        mathData.push(analytics());
    });
    let dataFiltered = mathData.filter(knowledge => knowledge.rating >= process.env.LEARNING_PRECISION);
    if (dataFiltered) {
        return {
            status: 300,
            message:'Robot diz: Verifique os conhecimentos acumulados, existe algo similar.'
        }
    } else {
        return {
            status: 200,
            message:'Robot diz: Menssagens podem ser aprendidas.'
        }
    }
};

module.exports = {
    verifyKnowledge,
    insertKnowledge
};
