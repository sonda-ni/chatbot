const
    dashboard = require('../../../database/models/dashboard'),
    webPageUser = require('../../../database/models/webpageuser'),
    facebookUser = require('../../../database/models/facebookuser');



const updatePage = async () => {
    let totalUsers = await TotalUsers();
    let facebook = await facebookUser.estimatedDocumentCount({}, function (err, count) {});
    return dashboard.findById(1).then(result => {
        return {
            data:{
                messages: thousandToKFormatter(result._doc.messages),
                facebook: thousandToKFormatter(result._doc.facebook),
                sobretre: thousandToKFormatter(result._doc.sobretre),
                webpage: thousandToKFormatter(result._doc.webpage),
                whatsapp: thousandToKFormatter(result._doc.whatsapp),
                trelocal: thousandToKFormatter(result._doc.trelocal),
                tretitulo: thousandToKFormatter(result._doc.tretitulo),
                treregularidade: thousandToKFormatter(result._doc.treregularidade),
                keras: thousandToKFormatter(result._doc.keras),
                no_reply: thousandToKFormatter(result._doc.no_reply),
                notreplied: thousandToKFormatter(result._doc.notreplied),
                averagemessages: thousandToKFormatter(result._doc.messages / totalUsers),
            },
            facebook: facebook,
            totalUsers: totalUsers
        }
    });
};

const TotalUsers = async () => {
    try {
        let usersWebPage = await webPageUser.estimatedDocumentCount({}, function (err, count) {});
        let usersFacebook = await facebookUser.estimatedDocumentCount({}, function (err, count) {});
        return thousandToKFormatter(usersWebPage + usersFacebook);
    }catch (e) {
        console.log(`Documents Math Error: ${e}`)
    }
};

function thousandToKFormatter(x) {
    if(isNaN(x)) return x;

    if(x < 9999) {
        return x;
    }

    if(x < 1000000) {
        return Math.round(x/1000) + "K";
    }
    if( x < 10000000) {
        return (x/1000000).toFixed(2) + "M";
    }

    if(x < 1000000000) {
        return Math.round((x/1000000)) + "M";
    }

    if(x < 1000000000000) {
        return Math.round((x/1000000000)) + "B";
    }

    return "1T+";
}

module.exports = {
    TotalUsers,
    updatePage
    //TotalCalls
};