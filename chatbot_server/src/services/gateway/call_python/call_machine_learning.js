const
    axios = require('axios'),
    knowledge_database = require('../../dice_coeficiente');

let train_machine_learning = async () => {
    let intents = await knowledge_database.get_dataBase()
    intents.push(require("../../../database/file_base.json"))
    let data = JSON.stringify({
        "intents": intents,
        "learning_rate": parseFloat(process.env.LEARNING_RATE),
        "epochs": parseInt(process.env.EPOCHS)
    });

    let config = {
        method: 'post',
        url: process.env.PYTHON + 'generate_model',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };

    return await axios(config)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error
        });

}

let predict_machine_learning = async (message) => {
    let config = {
        method: 'post',
        url: process.env.PYTHON + 'predict',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            "message": message
        }
    };

    return await axios(config)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return {
                "status": "Python offline",
                "error": error
            }
        });
}

module.exports = {
    train_machine_learning,
    predict_machine_learning
}
