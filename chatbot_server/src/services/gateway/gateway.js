const
    tretitulo = require("./ml_start_functions/titulo"),// EVAL
    trelocal = require("./ml_start_functions/local"),// EVAL
    treregularidade = require("./ml_start_functions/regularidade"),// EVAL

    mongooseFind = require("../../database/robot/crud_user/find"),
    dashboardData = require('../../database/models/dashboard'),
    user_update = require('../../database/robot/crud_user/update'),
    knowledge = require("../../database/models/knowledge"),
    call_keras = require("./call_python/call_machine_learning"),
    sendFacebookMessage = require("../../routes/facebook/facebook_out");


let gateway_flow = async (input) => {
    //Data
    update_dashboard(input)
    let document = await get_user(input)
    document.chat.messages.push({"user": input['message'], "time": Date.now()});

    // Bot
    let validation = await trigger_functions(document, {'message': input['message']})
    return await bot(validation, input, document);
}

async function bot(validation, input, document) {
    if (!validation.queue) {

        // Call Python if queue is false
        let response_data = await machine_learning(input);
        update_user(document, response_data)


        // If Python is off
        if (response_data.status) return {"message": "Estamos Fora do Ar"}


        // If bot do not recognize trigger save one more call at no_reply
        if (response_data["tag"] === "no_reply"){
            // Save data
            dashboardData.findOneAndUpdate({_id: 1}, {$inc: {no_reply: 1}}).then(data => {})
                .then(data => {})
        }

        // Add one call to the called knowledge
        knowledge.findOneAndUpdate({subject: response_data["tag"]}, {$inc: {calls: 1}})
            .then(data => {})


        // Trigger function if exists
        if (response_data['bot_function'] === 'No Functions'  || !response_data['bot_function']) {
            dashboardData.findOneAndUpdate({_id: 1}, {$inc: {keras: 1}}).then(data => {})
            return gateway_answer(response_data['message'], input)
        } else {
            // Save data
            eval(`dashboardData.findOneAndUpdate({_id: 1}, {$inc: {${response_data['bot_function']}: 1}})`)
                .then(data => {})
            // Trigger function
            return await eval(`${response_data['bot_function']}(response_data['message'], document)`).then(result => {
                return gateway_answer(result.text, input)
            })
        }


    } else return gateway_answer(validation.message, input)
}

// Prepare message to proper way out
async function gateway_answer(message, input) {
    if (input['collection'] === "facebook"){
        sendFacebookMessage(input['user'], message, input['PAGE_ACCESS_TOKEN']);
        return 200
    }else if (input['collection'] === "webpage"){
        return {'message': message}
    }
}

// Verify queue
let trigger_functions = async (document, response_data = "No") => {
    let queue = false;
    for (const key of Object.keys(document.chatsteps)) {
        // Trigger if queue is true
        if (document.chatsteps[key] > 0) {
            if (key !== "chaterror") {
                queue = true;
                return await eval(`${key}(response_data['message'], document)`).then(result => {
                    return {
                        "queue": queue,
                        "message": result.text
                    }
                });
            }
        }
    }
    return {
        "queue": queue
    };
}


let machine_learning = (input) => {
    return call_keras.predict_machine_learning(input['message'])
}

let update_user = (document, response_data) => {
    let data = {
        "document": document
    }
    user_update(data, document['platform'], response_data)
}


let get_user = async (input) => {
    return await mongooseFind(input['user'], input['collection'], input['PAGE_ACCESS_TOKEN'])
}

function update_dashboard(input) {
    Promise.all([
        dashboardData.findOneAndUpdate({_id: 1}, {$inc: {messages: 1}}),
        eval(`dashboardData.findOneAndUpdate({_id: 1}, {$inc: {${input.collection}: 1}})`)
    ]).then(r => {})
}

module.exports = {gateway_flow}
