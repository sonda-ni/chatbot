const
    regularizeSentence = require("../../dice_coeficiente/string-analytics/regularizeSentence"),
    dashboardData = require('../../../database/models/dashboard'),
    update_user_database = require("../../../database/robot/crud_user/update"),
    nameValidator = require("./validator/namevalidator"),
    axios = require('axios'),
    dataValidator = require("./validator/datevalidation");
require('dotenv').config();

/*
LOCAL
This module requires a form to be fulfilled by the user in
order to get information from the Data Base.
 */
module.exports = async (wholeMessage, document) => {
    let splittedMessage = regularizeSentence(wholeMessage).split(" ");
    if (splittedMessage.find(word => word === "cancelar" || word === "sair" || word === "parar" || word === "chega")) {
        document.chatsteps.trelocal = 0;
        let num = Math.floor(Math.random() * 2);
        let arrayBotSay = [];
        arrayBotSay.push("Ótimo ! Estou a disposição!");
        arrayBotSay.push("Saindo... Se precisar estou aqui!");
        return prepareMessage(arrayBotSay[num], document);
    } else if (splittedMessage.find(word => word === "errado" || word === "errei" || word === "nao")) {
        document.chatsteps.trelocal = 1;
        let num = Math.floor(Math.random() * 2);
        let arrayBotSay = [];
        arrayBotSay.push("Tudo bem... vou perguntar novamente. Qual é o seu nome COMPLETO?");
        arrayBotSay.push("Então vamos começar novamente! Qual é o seu nome COMPLETO?");
        return prepareMessage(arrayBotSay[num], document);
    } else {
        return await proceed(document, wholeMessage);
    }
};

const proceed = (document, wholeMessage) => {

    let botSay;
    switch (document.chatsteps.trelocal) {
        case 0:
            let num = Math.floor(Math.random() * 2);
            let arrayBotSay = [];
            arrayBotSay.push(`Para saber qual é o local de votação preciso fazer 3 perguntas, caso queira SAIR do formulário basta me dizer! 
            Informe o seu nome COMPLETO.`);
            arrayBotSay.push(`Sobre o local de votação preciso fazer 3 perguntas, caso queira SAIR do formulário basta me dizer! 
            Informe o seu nome COMPLETO.`);
            document.chatsteps.trelocal = 1;
            document["botsay"] = arrayBotSay[num];
            return prepareMessage(document.botsay, document);

        case 1:
            if (nameValidator(regularizeSentence(wholeMessage))) {
                document.nome = regularizeSentence(wholeMessage).toString().toUpperCase();
                botSay = `NOME: ${document.nome} 
                Agora informe sua data de nascimento da seguinte forma: DD/MM/AAAA`;
                document.chatsteps.trelocal = 2;
                document["botsay"] = botSay;
                return (prepareMessage(document.botsay, document));
            } else {
                document["botsay"] = "Por favor, coloque um nome válido.";
                return (prepareMessage(document.botsay, document));
            }
        case 2:
            if (dataValidator(wholeMessage)) {
                document.dtNascimento = wholeMessage.split("/").reverse().join("-");
                botSay = `
                NOME: ${document.nome}, 
                NASCIMENTO: ${wholeMessage}, 
                agora informe o nome da sua mãe COMPLETO.`;
                document.chatsteps.trelocal = 3;
                document["botsay"] = botSay;
                return prepareMessage(document.botsay, document);
            } else {
                document["botsay"] = "Por favor, coloque uma data válida.";
                return (prepareMessage(document.botsay, document));
            }
        case 3:
            return ifValidDataReturn(wholeMessage, document);
    }
    document["sent"] = false;
};


const fillForm = (document, treJson) => {
    document.nome = treJson.nome;
    document.titulodeeleitor = treJson.id;
    document.nomeMae = treJson.nomeMae;
    document.dtNascimento = treJson.dtNascimento;
    document.situacaoeleitoral = treJson.situacao;
    document.numZona = treJson.numZona;
    document.municipio = treJson.municipio;
    document.bairro = treJson.bairro;
    document.enderecoZona = treJson.enderecoZona;
    document.localVotacao = treJson.localVotacao;
    document.enderecoLocal = treJson.enderecoLocal;
    document.numSecao = treJson.numSecao;

    // Location Google
    document.latitude = treJson.latitude || null;
    document.longitude = treJson.longitude || null;

    return document;
};

async function ifValidDataReturn(wholeMessage, document) {
    let botSay;
    if (nameValidator(regularizeSentence(wholeMessage))) {
        document.nomeMae = regularizeSentence(wholeMessage).toString().toUpperCase();
        return await axios.get(process.env.BDTRE, {
            params: {
                nome: document.nome,
                nomeMae: document.nomeMae,
                dtNascimento: document.dtNascimento
            }
        }).then(response => {
            let documentOut = fillForm(document, response.data);
            if (documentOut.titulodeeleitor !== "") {
                if (document.situacaoeleitoral.toUpperCase() === "REGULAR") {
                    botSay = `
                *LOCAL* 
                ${document.platform === "webpage" ? `<br>`: ""}
                Eleitor: ${document.nome}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Título: ${document.titulodeeleitor}. 
                ${document.platform === "webpage" ? `<br>`: ""} 
                Local: ${document.localVotacao}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Zona: ${document.numZona}. 
                ${document.platform === "webpage" ? `<br>`: ""} 
                Endereço: ${document.enderecoLocal}.  
                Município: ${document.municipio}.  
                ${document.platform === "webpage" ? `<br>`: ""}  
                ${ document.latitude ? document.platform === "webpage" ? `<a href="Google Maps: https://www.google.com/maps/search/?api=1&query=${document.latitude},${document.longitude}">Google Maps<\a>`
                        : `Google Maps: https://www.google.com/maps/search/?api=1&query=${document.latitude},${document.longitude}`     : ""}
                ${document.platform === "webpage" ? `<br>`: ""}  
                -> Você está regular com o TRE.`;
                } else {
                    botSay = `
                *LOCAL*  
                ${document.platform === "webpage" ? `<br>`: ""}
                Eleitor: ${document.nome}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Título: ${document.titulodeeleitor}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Local: ${document.localVotacao}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Zona: ${document.numZona}.  
                ${document.platform === "webpage" ? `<br>`: ""}
                Endereço: ${document.enderecoLocal}.  
                Município: ${document.municipio}.  
                ${document.platform === "webpage" ? `<br>`: ""}  
                ${ document.latitude ? document.platform === "webpage" ? `<a href="Google Maps: https://www.google.com/maps/search/?api=1&query=${document.latitude},${document.longitude}">Google Maps<\a>`
                        : `Google Maps: https://www.google.com/maps/search/?api=1&query=${document.latitude},${document.longitude}`     : ""} 
                ${document.platform === "webpage" ? `<br>`: ""}  
                -> Você não está regular conosco, procure o cartório eleitoral para regularizar sua situação. Endereço: ${document.enderecoZona}`;
                }
                documentOut.chatsteps.trelocal = 0;
                document["sent"] = true;
                dashboardData.findOneAndUpdate({_id: 1}, {$inc: {trelocal: 1}})
                    .then(() => {
                    })
                    .catch(() => {
                    });
                return prepareMessage(botSay, documentOut)
            }

        }).catch(function (error) {
            if (error.code === "ECONNREFUSED") {
                document.chatsteps.tretitulo = 0;
                document["botsay"] = `Serviço fora do ar.`;
                return prepareMessage(document.botsay, document);
            } else {
                document.chatsteps.tretitulo = 0;
                document["botsay"] = `O título eleitoral não foi localizado no Cadastro Eleitoral.`;
                return prepareMessage(document.botsay, document);
            }
        });

    } else {
        document["botsay"] = "Por favor, coloque um nome válido.";
        return (prepareMessage(document.botsay, document));
    }
}

const prepareMessage = (text, document) => {
    update_user(document, text)
    document["sent"] = true;
    return {
        text: text,
        document: document
    }
};

let update_user = (document, text) => {
    let data = {
        'document': document
    }
    update_user_database(data, document['platform'], {'message': text})
}