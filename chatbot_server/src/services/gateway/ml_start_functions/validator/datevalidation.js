module.exports = inputText => {
    //let regEx = /^\d{4}-\d{2}-\d{2}$/; north america
    let regEx = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    return inputText.match(regEx);
};