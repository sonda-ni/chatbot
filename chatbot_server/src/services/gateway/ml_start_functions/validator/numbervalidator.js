module.exports = inputText => {
    let numberRegex = /^\s*[+-]?(\d+|\.\d+|\d+\.\d+|\d+\.)(e[+-]?\d+)?\s*$/;
    return inputText.match(numberRegex);
};