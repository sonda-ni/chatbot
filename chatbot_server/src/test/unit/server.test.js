const request = require('supertest');

const app = require('../../app'),
    facebookJson = require('./facebook.request'),
    mongoose = require('mongoose');
app.use('', require('../../routes/routes'));

let uri = process.env.MONGODB;
let mongoStats;
let toolToken = '';
let connection = 'Error';
let options = {
    dbName: process.env.DBNAME,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
    //user: process.env.USER,
    //pass: process.env.PASS
};


beforeAll(async () => {
    await mongoose.connect(uri, options)
        .then((result) => {
            mongoStats = result;
            connection = 'Mongo connected';
            console.log(`MongoDB Connected`)
        })
        .catch(error => console.log(`MongoDB Connection Error  -> Verify connect.js, DB or authentication
    LOG: ${error}`));

        let test = await request(app)
            .post('/login')
            .send({
                email: process.env.USER,
                password: process.env.PASS
            });
        toolToken = test.res.headers['set-cookie'][0];

});
afterAll(async () => {
    await mongoose.disconnect();
});


describe('MongoDB Connection', () => {
    it('1- Connection Test', async function (done) {
        expect(connection).toBe('Mongo connected');
        done();
    });
});

describe('Robot Test', () => {
    it('1- Robot Standard Message Test', async function (done) {
        let test = await request(app)
            .post('/dice_coeficiente')
            .send({
                user: 'renan.fernandes@ctis.com', // process.env.USER
                message: "Oi!"
            })
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /text/)
            .expect(200);
        expect(test.text).toBe("Oi! Em que posso ajudar?");
        done()
    });


    it('2- Robot Get Data A.I. From Message Test', async function (done) {
        let test = await request(app)
            .post('/dice_coeficiente/data')
            .send({
                user: 'renan.fernandes@ctis.com',
                message: "Como votar"
            })
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body).toHaveProperty(['output']);
        done()
    });

    it('3- Robot Insert New Knowledge', async function (done) {
        let test = await request(app)
            .put('/dice_coeficiente/knowledgeia')
            .send(JSON.stringify({
                "subject": "Jest Test",
                "inputs": ["Jeste testing...", "Jest testing 2..."],
                "key": "jest",
                "output": ["JEST !", "Jest 2!"],
                "summary": "Jest testing knowledge"
            }))
            .set('Cookie', toolToken)
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /text/)
            .expect(200);

        expect(typeof test.text).toBe('string');
        done()
    });

    it('4- Robot I.A. Search Knowledge', async function (done) {
        let test = await request(app)
            .search('/dice_coeficiente/knowledgeia')
            .send({"search": "Jeste testing..."})
            .set('Content-Type', 'application/json')
            .set('Cookie', toolToken)
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body.list[0]).toHaveProperty('keysrating');
        done()
    });

    it('5- Robot Search Knowledge by Subject', async function (done) {
        let test = await request(app)
            .search('/dice_coeficiente/knowledgesubject')
            .send({"search": "Jest Test"})
            .set('Cookie', toolToken)
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body).toHaveProperty('list');
        done()
    });
});

describe('Server Test', () => {

    it('1- Mongo Connection Test', async function (done) {
        await expect(mongoStats.connections[0]._readyState).toBe(1);
        done()
    });

    it('2- Facebook Test - Calls and ENV', async function (done) {
        expect(typeof process.env.APP_SECRET).toBe('string');
        expect(typeof process.env.PAGE_ACCESS_TOKEN).toBe('string');
        expect(typeof process.env.VALIDATION_TOKEN).toBe('string');

        await request(app)
            .post('/facebook')
            .send(facebookJson())
            .set('Accept', 'application/json')
            .expect(200);
        done()
    });

    it('3- DOTENV Variables Test', async function (done) {

        expect(typeof process.env.PORT).toBe('string');
        expect(typeof process.env.LOCALSERVER).toBe('string');
        expect(typeof process.env.BDTRE).toBe('string');
        expect(typeof process.env.MONGODB).toBe('string');
        expect(typeof process.env.DBNAME).toBe('string');
        expect(typeof process.env.USER_NAME).toBe('string');
        expect(typeof process.env.USER).toBe('string');
        expect(typeof process.env.PASS).toBe('string');
        expect(typeof process.env.MESSENGER_PRECISION).toBe('string');
        expect(typeof process.env.LEARNING_PRECISION).toBe('string');
        expect(typeof process.env.NODE_ENV).toBe('string');
        done()
    });
});

describe('Webtool Page', () => {

    it('1- Authentication', async function (done) {
        expect(toolToken).toContain('toolToken');
        done()
    });

    it('2- Login Page', async function (done) {
        await request(app)
            .get('')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('3- Dashboard Page', async function (done) {
        await request(app)
            .get('/webtool/dashboard')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('4- Create Admin Page', async function (done) {
        await request(app)
            .get('/webtool/newadmin')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('5- Machine Learning Page', async function (done) {
        await request(app)
            .get('/webtool/machinelearning')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('6- Robot Knowledge Page', async function (done) {
        await request(app)
            .get('/webtool/knowledge')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('7- Admin Administration Page', async function (done) {
        await request(app)
            .get('/webtool/admins')
            .set('Cookie', toolToken)
            .expect('Content-Type', /html/)
            .expect(200);
        done()
    });

    it('8- Cookie Session Test', async function (done) {
        await request(app)
            .get('/webtool/newadmin')
            .set('Cookie', '12345')
            .expect('Content-Type', /text/)
            .expect(302);
        done();
    });
});

describe('Users CRUD', () => {
    let insert = {
        _id: 'test@tre.com.br',
        firstName: 'Test',
        lastName: 'Creation',
        email: 'test@tre.com.br',
        password: '123',
        business: 'CRUD TEST',
        access: 'admin'
    };
    it('1- Create', async function (done) {
        let test = await request(app)
            .subscribe('/webtool/toolkit/admins')
            .send(insert)
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);

        expect(test.body.response).toBe('Admin Saved');
        done()
    });

    it('2- Refuse If Created', async function (done) {
        let test = await request(app)
            .subscribe('/webtool/toolkit/admins')
            .send(insert)
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);

        expect(test.body.response).toBe('User Exists');
        done()
    });

    it('3- Update', async function (done) {
        let test = await request(app)
            .merge('/webtool/toolkit/admins')
            .send(insert)
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body.response).toBe('Updated');
        done()
    });

    it('4- Read', async function (done) {
        let test = await request(app)
            .search('/webtool/toolkit/admins')
            .send(insert)
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body).toHaveProperty('list');
        done()
    });

    it('5- Delete', async function (done) {
        let test = await request(app)
            .delete('/webtool/toolkit/admins')
            .send(insert)
            .set('Cookie', toolToken)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200);
        expect(test.body).toHaveProperty('response');
        done()
    });
});


